# Automatic Debian binary package building for WMF

Build binary packages from `dgit`-prepared branches in CI. Optionally track updates from Debian (and then automatically attempt to incorporate them into WMF packages).

TL;DR: use this as the CI/CD configuration file setting:

```
builddebs.yml@repos/sre/wmf-debci
```

And have branches called e.g. `bookworm-wikimedia` to track updates to bookworm (specify suite to build for in `debian/changelog`).

For tracking of Debian updates, also push the relevant `dgit/*`
branch(es), make a [Project Access
Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token)
and store it as a masked CI variable called `DGIT_CI_TOKEN`. Then
[schedule a pipeline
run](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#add-a-pipeline-schedule)
for one of your `dgit/*` branches.

If your package build requires dependencies from the relevant -backports suite, then set a CI variable called `USEBACKPORTS` to any non-zero value. If you require additional components from the WMF apt repository, specify them as a space-seprated list in a CI variable called `EXTRACOMPONENTS`.

## What this does

Essentially, for any branch named `foo-wikimedia` or
`foo-wikimedia-*`, this arranges to pull the standard docker image for
the suite specified in `debian/changelog` from the WMF mirror, install
build-essential and the build-dependencies of the package, and attempt
a binary build.

Additionally, if you push a dgit branch `dgit/foo` for a recognised
Debian suite `foo`, then when that branch is updated (you can do this
manually, or [schedule a pipeline
run](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#add-a-pipeline-schedule)
to do it for you) due to an update of the package in that suite in
Debian, then this will attempt to incorporate those changes into every
tracking branch (named `foo-wikimedia` or `foo-wikimedia-*`) and
commit and push the result. This will automatically trigger an attempt
to build the new packages.

For this to be possible, the pipeline needs a suitable credential (the
default `CI_JOB_TOKEN` is read-only, though there is an [upstream
proposal to optionally grant it write
access](https://gitlab.com/gitlab-org/gitlab/-/issues/389060)), which
needs to be called `DGIT_CI_TOKEN`. Create one by making a [Project
Access
Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token)
and store it as a masked CI variable called `DGIT_CI_TOKEN`.

For more instructions on the package preparation side of this, see [Wikitech](https://wikitech.wikimedia.org/wiki/Debian_packaging_with_dgit_and_CI#); but broadly any git tree you started with `dgit clone` should be suitable.

## How to use it

The easiest thing is to just set your repository's CI/CD configuration file (under Settings-CI/CD-General pipelines) to `builddebs.yml@repos/sre/wmf-debci`. That uses the `builddebs.yml` file from this repo as your CI/CD config. There is also `wmfdeb.yml` which is aimed more for internal-only software (it attempts a build for every commit to the default branch where `debian/changelog` is updated).

Alternatively, if you want to use it as a starting point and extend / override it, you can instead create `debian/.wmf-gitlab-ci.yml` with these contents:

```yaml
---
include:
  - https://gitlab.wikimedia.org/repos/sre/wmf-debci/raw/main/includes.yml
```

And then set `debian/.wmf-gitlab-ci.yml` as the CI/CD configuration file. For examples of how `includes.yml` can be extended, see [`builddebs.yml`](builddebs.yml) and [`wmfdeb.yml`](wmfdeb.yml) in this repository. 

## Experimental / Work-In-Progress

The files `WIP.yml` and `WIPincludes.yml`, if present, are used for
experiments and development work; do not rely on them for production
workflows!